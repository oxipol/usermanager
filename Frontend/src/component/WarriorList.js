import React from "react";
import Warrior from "./Warrior";
import { Query } from "react-apollo";
import { GET_WARRIORS } from "../utils/gql_scema";
import { DELETE_WARRIOR } from "../utils/gql_scema";
import { graphql } from "react-apollo";

class WarriorList extends React.Component {
  render() {
    return (
      <Query query={GET_WARRIORS}>
        {({ loading, error, data }) => {
          if (loading) return <p> Loading...</p>;
          if (error) return <p> Oups...</p>;
          if (data.warriors.length === 0) {
            return <h3>Add your first cosmonaut!</h3>;
          } else {
            return data.warriors.map(warrior => {
              if (warrior.isDeleted) {
                this.props.DeleteWarrior({
                  variables: {
                    id: warrior.id
                  }
                });
              } else {
                return <Warrior key={warrior.id} warrior={warrior} />;
              }
            });
          }
        }}
      </Query>
    );
  }
}

export default graphql(DELETE_WARRIOR, { name: "DeleteWarrior" })(WarriorList);
