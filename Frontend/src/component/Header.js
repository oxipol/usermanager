import React from "react";
import react_logo from "../assets/react_logo.svg";
import gf_logo from "../assets/gf_logo.gif";
import blockchain_logo from "../assets/blockchain_logo.png";
import {
  HeaderWrapper,
  GFLogo,
  BlockchainLogo,
  ReactLogo
} from "../styles/HeaderStyles";

const Header = () => {
  return (
    <HeaderWrapper>
      <GFLogo src={gf_logo} alt="Gluten free logo" />
      cosmonaut manager on
      <BlockchainLogo src={blockchain_logo} alt="Blockchain logo" />
      technology, powered by
      <ReactLogo src={react_logo} alt="React spinning logo" />
    </HeaderWrapper>
  );
};

export default Header;
