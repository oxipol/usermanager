import React, { Component } from "react";
import { graphql } from "react-apollo";
import { EDIT_WARRIOR } from "../utils/gql_scema";
import ButtonWrapper from "../styles/ButtonWrapper";
import { checkDataValidation } from "../utils/DataValidation";

class SaveWarrior extends Component {
  handleOnClick = () => {
    const { isDeleted, id, nickname, name, DOB, superability } = this.props;
    if (checkDataValidation(nickname, name, DOB, superability)) {
      this.props.EditWarrior({
        variables: {
          id: id,
          nickname: nickname,
          name: name,
          DOB: DOB,
          superability: superability,
          isDeleted: isDeleted
        }
      });
      this.props.handleSave();
    }
  };

  render() {
    return (
      <ButtonWrapper type="submit" onClick={this.handleOnClick}>
        Save
      </ButtonWrapper>
    );
  }
}

export default graphql(EDIT_WARRIOR, { name: "EditWarrior" })(SaveWarrior);
