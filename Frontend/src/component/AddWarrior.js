import React from "react";
import { graphql } from "react-apollo";
import InputWrapper from "../styles/InputWrapper";
import ButtonWrapper from "../styles/ButtonWrapper";
import { ADD_WARRIOR } from "../utils/gql_scema";
import { GET_WARRIORS } from "../utils/gql_scema";
import { checkDataValidation } from "../utils/DataValidation";

class AddWarrior extends React.Component {
  state = { id: "", nickname: "", name: "", DOB: "", superability: "" };

  handleNickNameChange = event => {
    this.setState({ nickname: event.target.value });
  };
  handleNameChange = event => {
    this.setState({ name: event.target.value });
  };
  handleDOBChange = event => {
    this.setState({ DOB: event.target.value });
  };
  handleSuperabilityChange = event => {
    this.setState({ superability: event.target.value });
  };

  handleOnClick = () => {
    const { nickname, name, DOB, superability } = this.state;

    if (checkDataValidation(nickname, name, DOB, superability)) {
      this.props.AddWarrior({
        variables: {
          nickname: nickname,
          name: name,
          DOB: DOB,
          superability: superability
        },
        refetchQueries: [{ query: GET_WARRIORS }]
      });
      this.setState({ nickname: "", name: "", DOB: "", superability: "" });
    }
  };

  render() {
    const { nickname, name, DOB, superability } = this.state;
    return (
      <div>
        <InputWrapper
          value={nickname}
          placeholder="Enter nickname"
          maxLength="18"
          onChange={this.handleNickNameChange}
        />
        <InputWrapper
          value={name}
          placeholder="Enter name"
          maxLength="18"
          onChange={this.handleNameChange}
        />
        <InputWrapper
          value={DOB}
          placeholder="dd.mm.yyyy"
          maxLength="10"
          onChange={this.handleDOBChange}
        />
        <InputWrapper
          value={superability}
          placeholder="Enter superability"
          maxLength="18"
          onChange={this.handleSuperabilityChange}
        />
        <ButtonWrapper type="submit" onClick={this.handleOnClick}>
          Add
        </ButtonWrapper>
      </div>
    );
  }
}

export default graphql(ADD_WARRIOR, { name: "AddWarrior" })(AddWarrior);
