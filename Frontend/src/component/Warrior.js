import React from "react";
import SaveWarrior from "./SaveWarrior";
import InputWrapper from "../styles/InputWrapper";
import {
  WarriorWrapper,
  RestoreButtonWrapper,
  RestoreWrapper
} from "../styles/WarriorStyles";
import ButtonWrapper from "../styles/ButtonWrapper";
import { EDIT_WARRIOR } from "../utils/gql_scema";
import { graphql } from "react-apollo";

class Warrior extends React.Component {
  state = {
    isDeleted: false,
    isEdited: false,
    nickname: this.props.warrior.nickname,
    name: this.props.warrior.name,
    DOB: this.props.warrior.DOB,
    superability: this.props.warrior.superability
  };

  handleSave = () => {
    this.setState({ isEdited: false });
  };

  handleDelete = () => {
    this.setState({ isDeleted: true });
    this.props.EditWarrior({
      variables: {
        id: this.props.warrior.id,
        isDeleted: true
      }
    });
  };

  handleRestore = () => {
    this.setState({ isDeleted: false });
    this.props.EditWarrior({
      variables: {
        id: this.props.warrior.id,
        isDeleted: false
      }
    });
  };

  swapButton() {
    const {
      isDeleted,
      isEdited,
      nickname,
      name,
      DOB,
      superability
    } = this.state;
    if (!isEdited) {
      if (!isDeleted) {
        return (
          <ButtonWrapper type="submit" onClick={this.handleDelete}>
            Delete
          </ButtonWrapper>
        );
      } else {
        return (
          <RestoreButtonWrapper>
            <ButtonWrapper type="submit" onClick={this.handleRestore}>
              Restore
            </ButtonWrapper>
          </RestoreButtonWrapper>
        );
      }
    } else {
      return (
        <SaveWarrior
          id={this.props.warrior.id}
          nickname={nickname}
          name={name}
          DOB={DOB}
          superability={superability}
          handleSave={this.handleSave}
        />
      );
    }
  }

  handleDisplaying() {
    const { isDeleted, nickname, name, DOB, superability } = this.state;
    if (!isDeleted) {
      return (
        <div>
          <InputWrapper
            defaultValue={nickname}
            placeholder="Enter nickname"
            maxLength="18"
            onChange={this.handleNickNameChange}
          />
          <InputWrapper
            defaultValue={name}
            placeholder="Enter name"
            maxLength="18"
            onChange={this.handleNameChange}
          />
          <InputWrapper
            defaultValue={DOB}
            placeholder="Enter DOB"
            maxLength="10"
            onChange={this.handleDOBChange}
          />
          <InputWrapper
            defaultValue={superability}
            placeholder="Enter superability"
            maxLength="18"
            onChange={this.handleSuperabilityChange}
          />
        </div>
      );
    } else {
      return <RestoreWrapper>Cosmonaut is deleted. Restore?</RestoreWrapper>;
    }
  }

  handleEditing() {
    const { nickname, name, DOB, superability } = this.state;
    if (
      nickname === this.props.warrior.nickname &&
      name === this.props.warrior.name &&
      DOB === this.props.warrior.DOB &&
      superability === this.props.warrior.superability
    ) {
      this.setState({ isEdited: false });
    } else {
      this.setState({ isEdited: true });
    }
  }

  handleNickNameChange = event => {
    this.setState({ nickname: event.target.value }, () => this.handleEditing());
  };
  handleNameChange = event => {
    this.setState({ name: event.target.value }, () => this.handleEditing());
  };

  handleDOBChange = event => {
    this.setState({ DOB: event.target.value }, () => this.handleEditing());
  };
  handleSuperabilityChange = event => {
    this.setState({ superability: event.target.value }, () =>
      this.handleEditing()
    );
  };

  render() {
    return (
      <WarriorWrapper key={this.props.warrior.id}>
        {this.handleDisplaying()}
        {this.swapButton()}
      </WarriorWrapper>
    );
  }
}

export default graphql(EDIT_WARRIOR, { name: "EditWarrior" })(Warrior);
