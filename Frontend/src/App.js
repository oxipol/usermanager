import React from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import Header from "./component/Header";
import WarriorList from "./component/WarriorList";
import AddWarrior from "./component/AddWarrior";
import { MainWrapper, Border } from "./styles/AppStyles";

const client = new ApolloClient({
  uri: "http://localhost:3001/graphql"
});

const App = () => {
  return (
    <ApolloProvider client={client}>
      <MainWrapper>
        <Header />
        <Border>
          <WarriorList />
          <AddWarrior />
        </Border>
      </MainWrapper>
    </ApolloProvider>
  );
};

export default App;
