import gql from "graphql-tag";

export const GET_WARRIORS = gql`
  {
    warriors {
      id
      nickname
      name
      DOB
      superability
      isDeleted
    }
  }
`;

export const ADD_WARRIOR = gql`
  mutation addWarrior(
    $id: Int
    $nickname: String!
    $name: String!
    $DOB: String!
    $superability: String!
  ) {
    addWarrior(
      id: $id
      nickname: $nickname
      name: $name
      DOB: $DOB
      superability: $superability
    ) {
      id
    }
  }
`;

export const DELETE_WARRIOR = gql`
  mutation deleteWarrior($id: Int!) {
    deleteWarrior(id: $id) {
      nickname
    }
  }
`;

export const EDIT_WARRIOR = gql`
  mutation editWarrior(
    $id: Int!
    $nickname: String
    $name: String
    $DOB: String
    $superability: String
    $isDeleted: Boolean
  ) {
    editWarrior(
      id: $id
      nickname: $nickname
      name: $name
      DOB: $DOB
      superability: $superability
      isDeleted: $isDeleted
    ) {
      nickname
      name
      DOB
      superability
    }
  }
`;
