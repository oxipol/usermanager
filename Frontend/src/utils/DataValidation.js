export function checkDataValidation(nickname, name, DOB, superability) {
  const date_format = /^\d{1,2}\.\d{1,2}\.\d{4}$/;
  var tmp = DOB.split(".").map(Number);

  if (nickname === "") {
    alert("Enter nickname!");
    return false;
  }

  if (name === "") {
    alert("Enter name!");
    return false;
  }

  if (DOB === "") {
    alert("Enter date of birth!" + DOB);
    return false;
  } else {
    if (!DOB.match(date_format)) {
      alert("Invalid date format: " + DOB);
      return false;
    } else {
      if (tmp[0] < 1 || tmp[0] > 31) {
        alert("Invalid value for day: " + tmp[0]);
        return false;
      } else if (tmp[1] < 1 || tmp[1] > 12) {
        alert("Invalid value for month: " + tmp[1]);
        return false;
      } else if (tmp[2] < 1900 || tmp[2] > new Date().getFullYear()) {
        alert(
          "Invalid value for year: " +
            tmp[2] +
            " - must be between 1900 and " +
            new Date().getFullYear()
        );
        return false;
      }
    }
  }

  if (superability === "") {
    alert("Enter superability!");
    return false;
  }

  return true;
}
