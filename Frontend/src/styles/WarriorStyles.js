import styled from "styled-components";

export const WarriorWrapper = styled.div`
  display: flex;
  justify-content: center;
  color: white;

  @media (max-width: 450px) {
  }
`;

export const RestoreButtonWrapper = styled.div`
  margin-left: 587px;
  margin-bottom: 10px;

  @media (max-width: 450px) {
  }
`;

export const RestoreWrapper = styled.div`
  margin-top: 10px;

  @media (max-width: 450px) {
  }
`;
