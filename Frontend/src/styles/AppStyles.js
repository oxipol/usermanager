import styled from "styled-components";

export const MainWrapper = styled.div`
  text-align: center;
  padding: 20px;
  color: white;
  font-family: cursive;

  @media (max-width: 450px) {
  }
`;

export const Border = styled.div``;
