import styled, { keyframes } from "styled-components";

export const HeaderWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  font-size: 45px;
  border-bottom: 2px solid white;
  margin-bottom: 15px;

  @media (max-width: 450px) {
    
  }
`;

const ReactLogoSpin = keyframes`
  from { transform: rotate(0deg); }
  to { transform: rotate(360deg); }
`;

export const GFLogo = styled.img`
  height: 120px;
  padding: 20px;

  @media (max-width: 450px) {
    
  }
`;

export const BlockchainLogo = styled.img`
  height: 100px;
  padding: 20px;

  @media (max-width: 450px) {
    
  }
`;

export const ReactLogo = styled.img`
  height: 80px;
  animation: ${ReactLogoSpin} infinite 20s linear;

  @media (max-width: 450px) {
    
  }
`;
