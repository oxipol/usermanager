import styled from "styled-components";

const InputWrapper = styled.input`
  width: 188px;
  height: 23px;
  padding: 9px;
  overflow: hidden;
  border: none;
  border-radius: 7px;
  font: normal 17px / normal "Comic Sans MS", cursive, sans-serif;
  color: #039bd3;
  text-indent: 5px;
  text-overflow: clip;
  letter-spacing: 1px;
  background: #222;
  box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.2) inset;
  text-shadow: 2px 2px 6px rgba(22, 61, 255, 0.5);

  &::-webkit-input-placeholder {
    color: #61dafb;
  }

  @media (max-width: 450px) {
  }
`;

export default InputWrapper;
