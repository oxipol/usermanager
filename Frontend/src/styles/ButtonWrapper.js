import styled from "styled-components";

const ButtonWrapper = styled.button`
  width: 85px;
  height: 30px;
  margin-left: 5px;
  border: none;
  border-radius: 35px;
  font: normal 17px / normal "Comic Sans MS", cursive, sans-serif;
  color: #222;
  text-align: center;
  background: #61dafb;
  box-shadow: 2px 3px 4px 0 rgba(0, 0, 0, 0.3);
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
  outline: none;

  &:hover {
    background: #039bd3;

    @media (max-width: 450px) {
    }
  }
`;

export default ButtonWrapper;
