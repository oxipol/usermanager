var express = require("express");
var expressGraphQl = require("express-graphql");
var schema = require("./schema.js");
var cors = require("cors");

var app = express();

app.use(cors());

app.use(
  "/graphql",
  expressGraphQl({
    schema: schema,
    graphiql: true
  })
);

app.listen(3001, () => console.log("Now browse to localhost:3001/graphql"));
