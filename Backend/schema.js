const axios = require("axios");
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLSchema,
  GraphQLList,
  GraphQLNonNull,
  GraphQLBoolean
} = require("graphql");

const WarriorType = new GraphQLObjectType({
  name: "Warrior",
  fields: () => ({
    id: { type: GraphQLInt },
    nickname: { type: GraphQLString },
    name: { type: GraphQLString },
    DOB: { type: GraphQLString },
    superability: { type: GraphQLString },
    isDeleted: { type: GraphQLBoolean }
  })
});

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    warrior: {
      type: WarriorType,
      args: {
        id: { type: GraphQLInt }
      },
      resolve(parentValue, args) {
        return axios
          .get("http://localhost:3000/warriors/" + args.id)
          .then(res => res.data);
      }
    },
    warriors: {
      type: new GraphQLList(WarriorType),
      resolve(parentValue, args) {
        return axios
          .get("http://localhost:3000/warriors")
          .then(res => res.data);
      }
    }
  }
});

const mutation = new GraphQLObjectType({
  name: "Mutations",
  fields: {
    addWarrior: {
      type: WarriorType,
      args: {
        id: { type: GraphQLInt },
        nickname: { type: new GraphQLNonNull(GraphQLString) },
        name: { type: new GraphQLNonNull(GraphQLString) },
        DOB: { type: new GraphQLNonNull(GraphQLString) },
        superability: { type: new GraphQLNonNull(GraphQLString) }
      },
      resolve(parentValue, args) {
        return axios
          .post("http://localhost:3000/warriors", {
            id: args.id,
            nickname: args.nickname,
            name: args.name,
            DOB: args.DOB,
            superability: args.superability,
            isDeleted: false
          })
          .then(res => res.data);
      }
    },
    deleteWarrior: {
      type: WarriorType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLInt) }
      },
      resolve(parentValue, args) {
        return axios
          .delete("http://localhost:3000/warriors/" + args.id)
          .then(res => res.data);
      }
    },
    editWarrior: {
      type: WarriorType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLInt) },
        nickname: { type: GraphQLString },
        name: { type: GraphQLString },
        DOB: { type: GraphQLString },
        superability: { type: GraphQLString },
        isDeleted: { type: GraphQLBoolean }
      },
      resolve(parentValue, args) {
        return axios
          .patch("http://localhost:3000/warriors/" + args.id, args)
          .then(res => res.data);
      }
    }
  }
});

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation
});
