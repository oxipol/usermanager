# Manager demo

## Requirements

- yarn

## Run 

### Express  and JSON servers

.../Backend
```sh
yarn
yarn run dev:server
yarn run json:server
```

### Dev server

.../Frontend
```sh
yarn
yarn start
```

## Build for production

```sh
yarn
yarn build
```